# River Delta Data Vis

##### An interactive info graphic about floods and droughts in South Asia

## [Demo][demo]

## About

This website was designed by three undergraduate students at the [City College of New York][ccny] as part of a course in the Fall 2014 semester. The name of the course was Data Visualization, and it was taught by [Professor Michael Grossberg][grossberg] of the Computer Science department.

The foundation of this website is the flood data that was used in the 'Seasonal Effects' and 'Flood Factors' page sections. This data was provided by [Zachary Tessler][zach], a post graduate researcher at the [CUNY Environmental CrossRoads Initiative][crossroads]. Zachary focuses on environmental change in river deltas and implications for natural and human systems in the coastal zone. The website designers were very fortunate to have Zachary as their domain expert and mentor for this project.

An early version of this website was presented in class on December 11 2014. The slides from that presentation can be [downloaded here][slides].

## Screenshots

This is what the website looks like (for more screenshots checkout [this gallery][screenshot_gallery]):
![Screenshot 1][screenshot_1]
![Screenshot 1][screenshot_2]
![Screenshot 1][screenshot_3]

## Installation (for website)

NOTE:

* The website was successfully tested on Firefox (ver. 43) and Google Chrome (ver. 47)
* A window resolution of size 1024x768 or larger is recommended.

```bash
git clone https://github.com/ian-s-mcb/river-delta-data-vis
cd river-delta-data-vis/website
wget https://bitbucket.org/ian_s_mcb/data-vis-delta-project/downloads/json_2014-12-31.zip
wget https://bitbucket.org/ian_s_mcb/data-vis-delta-project/downloads/images_2014-12-31-b.zip
unzip json_2014-12-31.zip
unzip images_2014-12-31-b.zip
python -m SimpleHTTPServer 1234
# open localhost:1234 in firefox
```

## Exploring Our Data With Python

NOTE: This step requires:

* [Python][py] - our code was testing with Python ver. 2.7.6
* [iPython Notebook][ipynb] - the web-based interactive Python environment
* [NumPy][numpy] - the Python library for scientific computing
* [Pandas][pandas] - the Python library for data analysis

```bash
cd river-delta-data-vis/exploratory_files
wget https://bitbucket.org/ian_s_mcb/data-vis-delta-project/downloads/data_2014-12-31.zip
unzip data_2014-12-31.zip
mkdir figures json
cp ../website/json/ganges-articles.json ../website/json/mekong-articles.json json
ipython notebook
# in your browser, view the files 'flood_factors.ipynb' and 'seasonal_effects.ipynb'
# follow the code comment instructions
```

## Technologies We Used

* [Python][py] / [iPython Notebook][ipynb] - for data exploration
* [D3.js][d3] - for data visualization (in the browser)
* [Twitter Bootstrap][bootstrap] - for clean, responsive html/css features
* [AngularJS][angular] - for a minimal amount of html element hiding/revealing
* [jQuery][jquery] - for a few html element selections (out of a sheer laziness)

## Credits

* Andrew Fitzgerald - [Bitbucket profile][fitz_bb]
* Ian S. McBride - [GitHub profile][ian_gh] - [Bitbucket profile][ian_bb]
* Ebenezer Reyes - [Bitbucket profile][benny_bb]

[ccny]: http://www.ccny.cuny.edu/
[grossberg]: http://www.ccny.cuny.edu/profiles/Michael-Grossberg.cfm
[zach]: http://zacharytessler.com/
[crossroads]: http://rose.ccny.cuny.edu/~darlene/CrossRoads/
[slides]: https://bitbucket.org/ian_s_mcb/data-vis-delta-project/downloads/data-vis-delta-project_slides_2014-12-11.pdf
[demo]: http://ian-s-mcb.github.io/river-delta-deployment
[screenshot_gallery]: http://imgur.com/a/FKKcb
[screenshot_1]: http://i.imgur.com/gQwDeum.png
[screenshot_2]: http://i.imgur.com/AqWyuNb.png
[screenshot_3]: http://i.imgur.com/Mc1Wj4C.png
[py]: https://www.python.org/
[ipynb]: http://ipython.org/notebook.html
[numpy]: http://www.numpy.org/
[pandas]: http://pandas.pydata.org/
[d3]: http://d3js.org/
[bootstrap]: http://getbootstrap.com/
[angular]: https://angularjs.org/
[jquery]: https://jquery.org/
[fitz_bb]: https://bitbucket.org/fitzgeralda2010/
[ian_gh]: https://github.com/ian-s-mcb
[ian_bb]: https://bitbucket.org/ian_s_mcb/
[benny_bb]: https://bitbucket.org/bennywhf/
