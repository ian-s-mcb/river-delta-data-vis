// sets up angular features
angular
.module("vis", [])
.controller("seasonal-tabs", function() {

	this.active = "inundation";
	this.set = function(b) { this.active = b; };
	this.isSet = function(b) { return b === this.active; };
})
.controller("seasonal-toggles", function() {

	this.active = "ganges";
	this.set = function(delta) { this.active = delta; }
	this.isSet = function(delta) { return delta === this.active; }
})
.controller("factor-tabs", function() {

	this.active = "discharge";
	this.set = function(b) { this.active = b; };
	this.isSet = function(b) { return b === this.active; };
})
.controller("delta-toggles", function() {

	this.active = "ganges";
	this.set = function(delta) { this.active = delta; }
	this.isSet = function(delta) { return delta === this.active; }
});


// sets up seasonal visualizations contols
var setupSeasonalControls = function() {

	// shows first seasonal images
	$(".seasonal-images :first-child").css("display", "block");

	// sets initial position of seasonal sliders
	$(".seasonal-slider").val(0);

	// changes shown seasonal image depending upon slider tick position
	$(".seasonal-slider").on("input", function() {

		// element containing relevant images
		var el = this.previousElementSibling;

		// hides all relevant images
		$(el.children).css("display", "");

		// shows image corresponding to slider tick position
		$(el.children[$(this).val()]).css("display", "block");
	});
};


// draws a d3 plot for specified json file
var drawFactorPlot = function(dataFilename, div) {

	// plot interaction helper-1
	function showYear(yearName) {

		svg.select(".yr-" + yearName)
			.transition()
			.duration(600)
			.ease("linear")
			.style("opacity", 1);
		svg.select(".lt-" + yearName)
			.style("text-decoration", "none");

		//allow circles to change sidebar
		svg.selectAll(".circle-yr-" + yearName).attr("active", "active");
	}

	// plot interaction helper-2
	function hideYear(yearName) {

		svg.select(".yr-" + yearName)
			.transition()
			.duration(600)
			.ease("linear")
			.style("opacity", 0);
		svg.select(".lt-" + yearName)
			.style("text-decoration", "line-through")

		// prevents circles from changing sidebar
		svg.selectAll(".circle-yr-" + yearName).attr("active", "inactive");
	}

	// plot interaction helper-3
	function toggleYear(yearName) {

		var style = svg.select(".lt-" + yearName).style("text-decoration")
		style == "line-through" ? showYear(yearName) : hideYear(yearName);
	}

	// plot interaction helper-4
	function yearMouseover(yearName) {

		svg.select("g.yr-" + yearName + " path")
			.style("stroke-width", 4);
	}

	// plot interaction helper-5
	function yearMouseout(yearName) {

		svg.select(".yr-" + yearName + " path")
			.style("stroke-width", null);
	}

	// plot interaction helper-6
	function circleMouseover(circle, yearName, date, article) {

		// if circle is marked active
		if (svg.select(".circle-yr-" + yearName).attr("active") == "active") {

			// enlarges circle for emphasis
			circle.attr("r", 6);

			// fills sidebar with article and full date
			var factor = div.split("-")[1]; // remember that div is the element id for svg container
			var formatter = d3.time.format("%m-%d");
			var fullDate = yearName + "-" + formatter(date);
			fillSidebar(factor, article, fullDate);
		}
	}
	
	// plot interaction helper-7
	function circleMouseout(circle) {

		// restores circle to original size
		circle.attr("r", 4);
	}

	// plot interaction helper-8
	// fills sidebar with article info
	function fillSidebar(factor, article, date) {

		// prepares caption element
		var heading_el = "<h3>" + article.text + "</h3>";
		var link_el = '<a href="' + article.url + '">' + article.source + '</a>';
		var paragraph_el = "<p>Date: " + date + "</p><p>Source: " + link_el + "</p>";
		var caption_el = '<div class="caption">' + heading_el + paragraph_el + '</div>';

		// prepares image element
		var imgUrl = "images/article_images/" + article.image;
		var img_el = '<a href="' + article.url + '"><img src="' + imgUrl + '"></a>:';

		// selects sidebar
		// injects caption element and (if necessary) image element into sidebar
		d3.select("#" + factor + "-sidebar")
			.html(article.image ? caption_el + img_el : caption_el);
	}

	// prepares margins and dimensions
	var margin = {top: 20, right: 20, bottom: 100, left: 50},
		width = 600 - margin.left - margin.right,
		height = 370 - margin.top - margin.bottom;

	// prepares data formatter
	var parseDate = d3.time.format("%m-%d").parse;

	// prepares ranges for scales (x, y, and color)
	var x = d3.time.scale()
		.range([0, width]);
	var y = d3.scale.linear()
		.range([height, 0]);

	// prepares colors for years
	var color = d3.scale.ordinal().range(["#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2"]);

	// creates axes
	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom")
		.ticks(d3.time.months, 1)
		.tickFormat(d3.time.format("%b"));
	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	// prepares line helper
	var line = d3.svg.line()
		.interpolate("basis")
		.x(function(d) { return x(d.date); })
		.y(function(d) { return y(d.value); });

	// creates svg container
	var svg = d3.select(div).append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	// processes data
	d3.json(dataFilename, function(error, data) {

		// reads years dictionary (no rearranging needed)
		var years = data.years;

		// reads names of stat and year variables
		var statNames = d3.keys(data.stats[0]).filter(function(key) { return key !== "date"; });
		var yearNames = years.map(function(d) { return d.name;  });

		// maps year names to color scale
		color.domain(yearNames);

		// formats dates within stats in place
		data.stats.forEach(function(d) {
			d.date = parseDate(String(d.date));// NOTE: added casting to string to avoid error
		});
		years.forEach(function(y) {
			y.values.forEach(function(v) { v.date = parseDate(String(v.date)); });
		});

		// creates stats dictionary
		var stats = statNames.map(function(name) {
			return {
				name: name,
				values: data.stats.map(function(d) {
					return { date: d.date, value: +d[name] };
				})
			};
		});

		// sets the x and y domains
		x.domain(d3.extent(data.stats, function(d) { return d.date; }));
		y.domain([
			d3.min(data.stats, function(d) { return d.min; }),
			d3.max(data.stats, function(d) { return d.max; })
		]);

		// inserts axes
		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")")
			.call(xAxis);
		svg.append("g")
			.attr("class", "y axis")
			.call(yAxis)

		// creates shaded min/max area
		var area = d3.svg.area()
			.x(function(d) { return x(d.date); })
			.y0(function(d) { return y(d.min); })
			.y1(function(d) { return y(d.max); });
		svg.append("g")
			.attr("class", "stat-area")
		.append("path")
			.datum(data.stats)
			.attr("d", function(d) { return area(d); });

		// draws stats lines
		var stat = svg.selectAll(".stat")
			.data(stats)
			.enter()
			.append("g")
				.attr("class", function(d) { return "stat-" + d.name; });
		stat.append("path")
			.attr("class", "line")
			.attr("d", function(d) { return line(d.values); })
			.style("stroke", function(d) { return d.name == "avg" ? "black" : "gray" });

		// draws years lines
		var year = svg.selectAll(".year")
			.data(years)
			.enter()
			.append("g")
				.attr("class", function(d) { return "year yr-" + d.name; })
				.style("opacity", 0);
		var yearline = year.append("path")
			.attr("class", "line")
			.attr("d", function(d) { return line(d.values); })
			.style("stroke", function(d) { return color(d.name); })
			.on("mouseover", function(d) { yearMouseover(d.name);})
			.on("mouseout", function(d) { yearMouseout(d.name); });

		// adds circles to plot and article info to sidebar
		// loops over years
		year.each(function(d) {

			// current year element
			X = d3.select(this);

			// loops over dates of the current year
			d.values.forEach(function(c) {

				// if date has an article
				if (c.article !== null) {

					// adds a circle element
					X.append("circle")
						.attr({
							"class": function() { return "circle-yr-" + d.name; },
							"cx": x(c.date),
							"cy": y(c.value),
							"r": 4
						})
						.style("fill", function() { return color(d.name); })
						.on("mouseover", function() { circleMouseover(d3.select(this), d.name, c.date, c.article); })
						.on("mouseout", function() { circleMouseout(d3.select(this)); });
				}
			})
		});

		// uses year names as data for legend items
		var legendItem = svg.append("g")
			.attr("class", "legend")
			.style("transform", "translate(0, " + (height + 40) + "px)")
		.selectAll()
			.data(yearNames)
			.enter()
		.append("g")
			.attr("class", "legend-item")
			.style("transform", function(d, i) { return "translate(" + i * 35 + "px)"; })
			.on("click", function(d) { toggleYear(d); })
			.on("mouseover", function(d) { yearMouseover(d); })
			.on("mouseout", function(d) { yearMouseout(d); });

		// creates legend patches
		legendItem.append("circle")
			.attr({
				"class": "legend-patch",
				"r": 6
			})
			.style("fill", function(d) { return color(d); });

		// creates legend text
		legendItem.append("text")
			.text(function(d) { return d })
			.attr({
				"class": function(d) { return "legend-text lt-" + d; }
			})
			.style("text-decoration", "line-through");
	});
};


// init function
window.onload = function() {

	setupSeasonalControls();
	
	drawFactorPlot("json/ganges-discharge.json","#ganges-discharge");
	drawFactorPlot("json/ganges-inundation.json","#ganges-inundation");
	drawFactorPlot("json/ganges-precipitation.json","#ganges-precipation");
	drawFactorPlot("json/ganges-waves.json","#ganges-waves");

	drawFactorPlot("json/mekong-discharge.json","#mekong-discharge");
	drawFactorPlot("json/mekong-inundation.json","#mekong-inundation");
	drawFactorPlot("json/mekong-precipitation.json","#mekong-precipation");
	drawFactorPlot("json/mekong-waves.json","#mekong-waves");
};
